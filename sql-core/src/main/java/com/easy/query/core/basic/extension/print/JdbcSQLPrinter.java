package com.easy.query.core.basic.extension.print;

/**
 * create time 2024/8/25 12:52
 * 文件说明
 *
 * @author xuejiaming
 */
public interface JdbcSQLPrinter {
    Boolean printSQL();
}
